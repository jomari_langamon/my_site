import datetime
from django.http import HttpResponse
from django.template import Context
from django.template.loader import get_template
from django.shortcuts import render

def hello(request):
	return HttpResponse("Hello World")

def current_datetime(request):
  now = datetime.datetime.now()
  context = {'current_date':now}
  return render(
  		request,
  		'current_daytime.html',
  		context
  		)

def hours_ahead(request, offset):
    try:
        offset = int(offset)
    except ValueError:
        raise Http404()
    dt = {
    	'next_time':datetime.datetime.now() +
    		datetime.timedelta(hours=offset),
    	'hour_offset':offset
    }
    return  render(request, 'hours_ahead.html',dt)

def welcome(request):
	return HttpResponse("Welcome to Math!")

def add_nums(request, first, second):
    try:
        first = int(first)
        second = int(second)
    except ValueError:
        raise Http404()

	result = first + second
	return HttpResponse("The result of the equation {} + {} is {}!".format(first,second,result))

def mult_nums(request, first, second):
	try:
		first = int(first)
		second = int(second)
	except ValueError:
		raise Http404()

	result = first * second
	return HttpResponse("The result of the equation {} + {} is {}!".format(first,second,result))

def square_nums(request, number):
	try:
		number = int(number)
	except ValueError:
		raise Http404()

	result = number**2
	return HttpResponse("The square of {} is {}".format(number,result))