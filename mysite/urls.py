from django.conf.urls import patterns, include, url
from django.contrib import admin

admin.autodiscover()

from .views import hello, hours_ahead, current_datetime, welcome, add_nums, mult_nums, square_nums


urlpatterns = patterns('',
    url(r'^hello/$', hello),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^current_time/$', current_datetime),
    url(r'^time/plus/(\d+)/$', hours_ahead),
    url(r'^math/$', welcome),
    url(r'^add/(\d+)/(\d+)/$', add_nums),
    url(r'^mult/(\d+)/(\d+)/$', mult_nums),
    url(r'^square/(\d+)/$', square_nums),
)	